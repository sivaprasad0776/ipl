let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function matchesWonPerTeamPerYear() {
    try {
        const matchData = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');
        
        const matchesWonPerTeamPerYear = {};

        matchData.forEach((match)=>{
            let team = match["winner"];
            let season = match.season;

            if(!team || team.length == 0){
                return;
            }

            if(matchesWonPerTeamPerYear.hasOwnProperty(team)){
                let years = matchesWonPerTeamPerYear[team];
                if(years.hasOwnProperty(season)){
                    years[season] ++;
                }
                else{
                    years[season] = 1;
                }
            }
            else{
                let year = {}
                year[season] = 1;
                matchesWonPerTeamPerYear[team] = year;
            }

        });

        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/2-matches-won-per-team-per-year.json';
        const output = JSON.stringify(matchesWonPerTeamPerYear, null, " ");

        fs.writeFile(outputPath, output, (error)=>{
            if(error){
                console.log(error);
            }
        });
        
    } 
    catch (err) {
        console.log(err);
    }
};

matchesWonPerTeamPerYear(); 