let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function matchesPerYear() {
    try {
        const matchData = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');
        const matchesPerYear = {};

        matchData.forEach((match)=>{
            if(matchesPerYear.hasOwnProperty(match.season)){
                matchesPerYear[match.season] ++;
            }
            else{
                matchesPerYear[match.season] = 1;
            }

        });

        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/1-matches-per-year.json';
        const output = JSON.stringify(matchesPerYear, null, " ");

        fs.writeFile(outputPath, output, (error)=>{
            if(error){
                console.log(error);
            }
        });
        
    } 
    catch (error) {
        console.log(error);
    }
}

matchesPerYear(); 