let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function extraRunsConcededPerTeam(year) {
    try {
        const matches = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');
        const deliveries = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/deliveries.csv');

        const matchIdsInRequiredYear = matches.map((match) => {
            if(year == match.season){
                return match.id;
            }
        }).filter((id)=>{
            return id != undefined ;
        });

        let extraRunsConcededPerTeam = {};

        deliveries.forEach((delivery)=>{
            if(!matchIdsInRequiredYear.includes(delivery.match_id)){
                return;
            }
            let team = delivery.batting_team;

            if(extraRunsConcededPerTeam.hasOwnProperty(team)){
                extraRunsConcededPerTeam[team] += parseInt(delivery.extra_runs);
            }
            else{
                extraRunsConcededPerTeam[team] = parseInt(delivery.extra_runs);
            }

        });

        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/3-extra-runs-conceded-per-team-2016.json';
        const output = JSON.stringify(extraRunsConcededPerTeam, null, " ");

        fs.writeFile(outputPath, output, (error)=>{
            if(error){
                console.log(error);
            }
        });

    } catch (error) {
        console.log(error);
    }
}

extraRunsConcededPerTeam(2016);