let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function economicBowlers(year) {
    try {
        const matches = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');
        const deliveries = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/deliveries.csv');

        const matchIdsInRequiredYear = matches.map((match) => {
            if (year == match.season) {
                return match.id;
            }
        }).filter((id) => {
            return id != undefined;
        });

        const economicBowlers = {};

        deliveries.forEach((delivery) => {
            if (!matchIdsInRequiredYear.includes(delivery.match_id)) {
                return;
            }
            let bowler = delivery.bowler;

            let wideRuns = parseInt(delivery.wide_runs);
            let noballRus = parseInt(delivery.noball_runs);

            let ballCount = 1;
            if(wideRuns+noballRus > 0){
                ballCount = 0;
            }

            let byeRuns = parseInt(delivery.bye_runs);
            let legByeRuns = parseInt(delivery.legbye_runs);
            let totalRuns = parseInt(delivery.total_runs);
            let netRuns = totalRuns-(byeRuns+legByeRuns);

            if (economicBowlers.hasOwnProperty(bowler)) {
                let bowlingInfo = economicBowlers[bowler];
                bowlingInfo.runs += netRuns
                bowlingInfo.balls += ballCount;
            }
            else {
                let bowlingInfo = {};
                bowlingInfo["runs"] = netRuns
                bowlingInfo["balls"] = ballCount;
                economicBowlers[bowler] = bowlingInfo;

            }

        });

        const entries = Object.entries(economicBowlers);
        const top10Economies = entries.map((entry) => {
            let stats = entry[1];
            let economy = (stats.runs / (stats.balls / 6)).toFixed(2);
            let bowler = {};
            bowler['bowler'] = entry[0];
            bowler['economy'] = parseFloat(economy);
            return bowler;
        }).sort((b1, b2) => {
            return (b1.economy) - (b2.economy);
        }).slice(0, 10);


        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/4-top10-economical-bowlers-2015.json';
        const output = JSON.stringify(top10Economies, null, " ");

        fs.writeFile(outputPath, output, (error) => {
            if (error) {
                console.log(error);
            }
        });

    } catch (error) {
        console.log(error);
    }
}

economicBowlers(2015);