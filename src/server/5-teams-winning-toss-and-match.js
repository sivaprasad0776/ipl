let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function teamsWinningTossAndMatch() {
    try {
        const matchData = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');
        const teamsWinningTossAndMatches = {};

        matchData.forEach((match)=>{
            if(match.toss_winner == match.winner){
                if(teamsWinningTossAndMatches.hasOwnProperty(match.toss_winner)){
                    teamsWinningTossAndMatches[match.toss_winner] ++;   
                }
                else{
                    teamsWinningTossAndMatches[match.toss_winner] = 1;   
                }
            }
        });
       
        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/5-teams-winning-toss-and-match.json';
        const output = JSON.stringify(teamsWinningTossAndMatches, null, " ");

        fs.writeFile(outputPath, output, (error)=>{
            if(error){
                console.log(error);
            }
        }); 
        
    } 
    catch (err) {
        console.log(err);
    }
};

teamsWinningTossAndMatch(); 