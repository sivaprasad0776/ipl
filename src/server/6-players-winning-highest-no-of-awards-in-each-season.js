let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function matchesPerYear() {
    try {
        const matchData = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');

        const seasonWiseAwardsInfo = {};

        matchData.forEach((match) => {
            let player = match.player_of_match;
            let season = match.season;

            if (seasonWiseAwardsInfo.hasOwnProperty(season)) {
                let awardsInfo = seasonWiseAwardsInfo[season];
                if (awardsInfo.hasOwnProperty(player)) {
                    awardsInfo[player]++;
                }
                else {
                    awardsInfo[player] = 1;
                }
            }
            else {
                let awardsInfo = {};
                awardsInfo[player] = 1;
                seasonWiseAwardsInfo[season] = awardsInfo;
            }
        });

        const highestAwardsPerSeason = {};

        const awardsEntries = Object.entries(seasonWiseAwardsInfo);

        awardsEntries.forEach((awardEntry) => {
            let season = awardEntry[0];
            let awards = awardEntry[1];
            let highestEntry = Object.entries(awards).reduce((acc, entry) => {
                if (entry[1] > acc[1]) {
                    return entry;
                }
                else {
                    return acc;
                }
            }, ["temp", -1]);

            highestAwardsPerSeason[season] = Object.fromEntries([highestEntry]);

        });


        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/6-players-winning-highest-no-of-awards-in-each-season.js.json';
        const output = JSON.stringify(highestAwardsPerSeason, null, " ");

        fs.writeFile(outputPath, output, (error) => {
            if (error) {
                console.log(eror);
            }
        });

    }
    catch (error) {
        console.log(error);
    }
}

matchesPerYear(); 