let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function strikeRateOfBatsman() {
    try {
        const deliveries = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/deliveries.csv');
        const matchData = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/matches.csv');

        const seasonIds = {};

        matchData.forEach((match) => {
            seasonIds[match.id] = match.season;

        });

        const battingStats = {};

        deliveries.forEach((delivery) => {
            let id = delivery.match_id;
            let season = seasonIds[id];
            let player = delivery.batsman;
            let wide = parseInt(delivery.wide_runs);
            let noball = parseInt(delivery.noball_runs);
            let ballCount = 0;
            if ((wide + noball) == 0) {
                ballCount = 1;
            }
            let batsmanRuns = parseInt(delivery.batsman_runs);

            if (battingStats.hasOwnProperty(season)) {
                let players = battingStats[season];
                if (players.hasOwnProperty(player)) {
                    let runsStats = players[player];
                    runsStats['balls'] += ballCount;
                    runsStats['runs'] += batsmanRuns;
                }
                else {
                    let runsStats = { 'balls': ballCount, 'runs': batsmanRuns };
                    players[player] = runsStats;
                }
            }
            else {
                let playerStats = {};
                let runsStats = { 'balls': ballCount, 'runs': batsmanRuns };
                playerStats[player] = runsStats;
                battingStats[season] = playerStats;
            }

        });

        const batsmanEntries = Object.entries(battingStats);

        const finalStats = {};

        batsmanEntries.forEach((entry) => {
            let year = entry[0];
            let playersData = entry[1];
            let playerEntries = Object.entries(playersData);
            let economiesData = playerEntries.map((playerEntry) => {
                playerEntry[1] = parseFloat((playerEntry[1]["runs"] * 100 / playerEntry[1]["balls"]).toFixed(2));
                return playerEntry;
            });
            let obj = Object.fromEntries(economiesData);
            finalStats[year] = obj;
        });

        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/7-strike-rate-of-batsman-in-each-season.json';
        const output = JSON.stringify(finalStats, null, " ");

        fs.writeFile(outputPath, output, (error) => {
            if (error) {
                console.log(error);
            }
        });

    } catch (error) {
        console.log(error);
    }
}

strikeRateOfBatsman();