let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function highestDismissels() {
    try {
        const deliveries = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/deliveries.csv');

        const playersDismissals = {};

        deliveries.forEach((delivery)=>{
            let batsman = delivery.player_dismissed;

            if(!batsman || batsman.length == 0){
                return;
            }

            let kind = delivery.dismissal_kind;
            let bowler = delivery.bowler;
            let fielder = delivery.fielder;
            let dismissedBy = bowler;

            if (kind == "run out"){
                dismissedBy = fielder
            }

            if(!dismissedBy || dismissedBy.length == 0){
                return;
            }

            if(playersDismissals.hasOwnProperty(batsman)){
                let dismissers = playersDismissals[batsman];
                if(dismissers.hasOwnProperty(dismissedBy)){
                    dismissers[dismissedBy] ++;
                }
                else{
                    dismissers[dismissedBy] = 1;
                }
            }else{
                let obj={};
                obj[dismissedBy] = 1;
                playersDismissals[batsman] = obj;

            }

        });

        //console.log(playersDismissals);

        const dismissalEntries = Object.entries(playersDismissals);

        const finalStats = {};

        dismissalEntries.forEach((entry) => {
            let player = entry[0];
            let dismissers = entry[1];
            let dismissersEntries = Object.entries(dismissers);

            let highestDismisser = dismissersEntries.reduce((acc, currentEntry) => { 
                if(parseInt(currentEntry[1]) > acc[1]){
                    return currentEntry;
                }
                else{
                    return acc;
                }
            }, ["test", -1]);

            let obj = Object.fromEntries([highestDismisser]);

            finalStats[player] = obj;
        });

        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/8-highest-times-player-dismissed-by-other.json';
        const output = JSON.stringify(finalStats, null, " ");

        fs.writeFile(outputPath, output, (error)=>{
            if(error){
                console.log(error);
            }
        });

    } catch (error) {
        console.log(error);
    }
}

highestDismissels();