let CSVToJSON = require('csvtojson');
let fs = require('fs');

async function bestEconomicBowler() {
    try {
        const deliveries = await CSVToJSON().fromFile('/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/data/deliveries.csv');

        const bowlersEconomy = {};
        
        deliveries.forEach((delivery)=>{
            let superOver = delivery.is_super_over;
            if(superOver == 0){
                return;
            }

            let wideRuns = parseInt(delivery.wide_runs);
            let noballRus = parseInt(delivery.noball_runs);

            let ballCount = 1;
            if(wideRuns+noballRus > 0){
                ballCount = 0;
            }

            let byeRuns = parseInt(delivery.bye_runs);
            let legByeRuns = parseInt(delivery.legbye_runs);
            let totalRuns = parseInt(delivery.total_runs);
            let netRuns = totalRuns-(byeRuns+legByeRuns);

            let bowler = delivery.bowler;

            if(bowlersEconomy.hasOwnProperty(bowler)){
                let stats = bowlersEconomy[bowler];
                stats["balls"] += ballCount;
                stats["runs"] += netRuns;
            }
            else{
                let obj = {};
                obj["balls"] = ballCount;
                obj["runs"] = netRuns;
                bowlersEconomy[bowler] = obj;
            }
        });

        let bowlersEconomyEntries = Object.entries(bowlersEconomy);
        let bestEconomyInfo = bowlersEconomyEntries.reduce((acc, economy)=>{
            let curEconomy = (economy[1].runs/(economy[1].balls/6)).toFixed(2);
            let accEconomy = (acc[1].runs/(acc[1].balls/6)).toFixed(2);
            if(curEconomy < accEconomy){
                return economy;
            }
            else{
                return acc;
            }

        }, ["test", {"runs":100, "balls":6}]);

        let bestEconomyValue = (bestEconomyInfo[1].runs/(bestEconomyInfo[1].balls/6)).toFixed(2);

        let bestEconomyInSuperOvers={};
        bestEconomyInSuperOvers[bestEconomyInfo[0]] = parseFloat(bestEconomyValue);

        const outputPath = '/home/sivaprasad/mountblue/projects/js-ipl-data-project/src/public/output/9-best-economy-bowler-in-super-overs.json';
        const output = JSON.stringify(bestEconomyInSuperOvers, null, " ");

        fs.writeFile(outputPath, output, (error)=>{
            if(error){
                console.log(error);
            }
        }); 

    } catch (error) {
        console.log(error);
    }
}

bestEconomicBowler();